# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 14:09:16 2022

@author: ymk91659
"""

import eel
import sqlite3
from sqlite3 import Error
from datetime import datetime, timedelta
import pandas as pd
import scheduler
import ctypes
import calendar

db_file = "ETDShiftPlanner.db"
eel.init('web')

def get_display_name():
    GetUserNameEx = ctypes.windll.secur32.GetUserNameExW
    NameDisplay = 3
 
    size = ctypes.pointer(ctypes.c_ulong(0))
    GetUserNameEx(NameDisplay, None, size)
 
    nameBuffer = ctypes.create_unicode_buffer(size.contents.value)
    GetUserNameEx(NameDisplay, nameBuffer, size)
    return nameBuffer.value

@eel.expose
def getUser():
    name = get_display_name()
    if "," in name:
        nameParts = name.split(" ")
        name = nameParts[1] + " " + nameParts[0].replace(",","")
    return name

@eel.expose
def getShiftData():
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        cur = conn.cursor()
        cur.execute('''
                    SELECT 
                        A.DayOfWeek, A.Date, 
                        G.PrimaryInitials, G.SecondaryInitials, 
                        A.Operator1Initials, A.Operator2Initials, 
                        B.Operator1Initials, B.Operator2Initials, 
                        C.Operator1Initials, C.Operator2Initials, 
                        D.Operator1Initials, D.Operator2Initials, 
                        E.Operator1Initials, E.Operator2Initials, 
                        F.Operator1Initials, F.Operator2Initials, 
                        A.Comments 
                    FROM 
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R25Shifts where Time='AM') as A
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R25Shifts where Time='PM') as B
                    ON A.Date=B.Date
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R100Shifts where Time='AM') as C
                    ON A.Date=C.Date
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R100Shifts where Time='PM') as D
                    ON A.Date=D.Date
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R114Shifts where Time='AM') as E
                    ON A.Date=E.Date
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R114Shifts where Time='PM') as F
                    ON A.Date=F.Date
                    INNER JOIN
                        (SELECT Date,
                         CASE
                             when PrimaryID is Null 
                             THEN 
                                 '' 
                             ELSE
                                 (SELECT Initials from TVTStaff where TVTStaff.ID=PrimaryID)
                             END
                             as PrimaryInitials,
                             CASE
                                 when SecondaryID is Null 
                             THEN 
                                 '' 
                             ELSE
                                 (SELECT Initials from TVTStaff where TVTStaff.ID=SecondaryID)
                             END
                             as SecondaryInitials
                        FROM TVTOnCall) as G
                    ON A.Date=G.Date
                    ''')
        rows = cur.fetchall()
        global shiftData 
        shiftData = pd.DataFrame(columns=["Day", "Date", "Primary On Call", "Secondary On Call", "R25 Day Primary", "R25 Day Secondary", "R25 Night Primary", "R25 Night Secondary", "R100 Day Primary", "R100 Day Secondary", "R100 Night Primary", "R100 Night Secondary", "R114 Day Primary", "R114 Day Secondary", "R114 Night Primary", "R114 Night Secondary", "Comments"])
        for row in rows:
            shiftData = shiftData.append({shiftData.columns[i]: row[i] for i in range(len(row))}, ignore_index=True)
        conn.close()
        return rows
    except Error as e:
        print(e)

@eel.expose
def checkUpdates():
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        cur = conn.cursor()
        cur.execute('''
                    SELECT 
                        A.DayOfWeek, A.Date, 
                        G.PrimaryInitials, G.SecondaryInitials, 
                        A.Operator1Initials, A.Operator2Initials, 
                        B.Operator1Initials, B.Operator2Initials, 
                        C.Operator1Initials, C.Operator2Initials, 
                        D.Operator1Initials, D.Operator2Initials, 
                        E.Operator1Initials, E.Operator2Initials, 
                        F.Operator1Initials, F.Operator2Initials, 
                        A.Comments 
                    FROM 
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R25Shifts where Time='AM') as A
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R25Shifts where Time='PM') as B
                    ON A.Date=B.Date
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R100Shifts where Time='AM') as C
                    ON A.Date=C.Date
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R100Shifts where Time='PM') as D
                    ON A.Date=D.Date
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R114Shifts where Time='AM') as E
                    ON A.Date=E.Date
                    INNER JOIN
                        (SELECT 
                            Date, 
                            CASE
                                when Operator1ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator1ID)
                            END
                            as Operator1Initials,
                            CASE
                                when Operator2ID is Null 
                            THEN 
                                '' 
                            ELSE
                                (SELECT Initials from TVTStaff where TVTStaff.ID=Operator2ID)
                            END
                            as Operator2Initials,
                            Comments, 
                            CASE
                                cast(strftime('%w', Date) as integer) 
                                when 0 then 'Sunday'
                                when 1 then 'Monday' 
                                when 2 then 'Tuesday'
                                when 3 then 'Wednesday'
                                when 4 then 'Thursday'
                                when 5 then 'Friday'
                                else 'Saturday'
                            END
                            as DayOfWeek
                        FROM R114Shifts where Time='PM') as F
                    ON A.Date=F.Date
                    INNER JOIN
                        (SELECT Date,
                         CASE
                             when PrimaryID is Null 
                             THEN 
                                 '' 
                             ELSE
                                 (SELECT Initials from TVTStaff where TVTStaff.ID=PrimaryID)
                             END
                             as PrimaryInitials,
                             CASE
                                 when SecondaryID is Null 
                             THEN 
                                 '' 
                             ELSE
                                 (SELECT Initials from TVTStaff where TVTStaff.ID=SecondaryID)
                             END
                             as SecondaryInitials
                        FROM TVTOnCall) as G
                    ON A.Date=G.Date
                    ''')
        rows = cur.fetchall()
        global changes
        changes = []
        for i in range(len(rows)):
            row = rows[i]
            for j in range(2,17):
                if row[j] != shiftData.iloc[i][j]:
                    dt = datetime.strptime(row[1], "%Y-%m-%d %H:%M:%S")
                    changes.append((datetime.strftime(dt, "%d/%m/%Y"), j, row[j]))
        conn.close()
        if changes != []:
            getShiftData()
        return changes
    except Error as e:
        print(e)

@eel.expose
def getLastUpdated():
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        cur = conn.cursor()
        cur.execute("SELECT LastModified from Properties")
        
        conn.close()
    except Error as e:
        print(e)

@eel.expose
def getAvailableStaff(date, time):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        if time == "day":
            cur.execute("SELECT Initials from TVTStaff where ID not in (select TVTStaffID as ID from TVTCommitments where CommitmentDate=? and (Comment like '%day%' or Comment not like '%night%'))", (date,))
        else:
            cur.execute("SELECT Initials from TVTStaff where ID not in (select TVTStaffID as ID from TVTCommitments where CommitmentDate=? and (Comment like '%night%' or Comment not like '%day%'))", (date,))
        rows = cur.fetchall()
        conn.close()
        return rows
    except Error as e:
        print(e)

@eel.expose
def updateData(date, column, value):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        dt = datetime.strptime(date, "%d/%m/%Y")
        cur = conn.cursor()
        staffId = ""
        if value != "":
            cur.execute("select ID from TVTStaff where Initials=?", (value,))
            staffId = cur.fetchone()[0]
        else:
            value = "None"
        if column == "2":
            cur.execute("update TVTOnCall set PrimaryID=? where Date=?", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Primary On Call for date {} to {}".format(getUser(), date, value)))
        if column == "3":
            cur.execute("update TVTOnCall set SecondaryID=? where Date=?", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Secondary On Call for date {} to {}".format(getUser(), date, value)))
        if column == "4":
            cur.execute("update R25Shifts set Operator1ID=? where Date=? and Time='AM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Primary Day R25 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "5":
            cur.execute("update R25Shifts set Operator2ID=? where Date=? and Time='AM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Secondary Day R25 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "6":
            cur.execute("update R25Shifts set Operator1ID=? where Date=? and Time='PM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Primary Night R25 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "7":
            cur.execute("update R25Shifts set Operator2ID=? where Date=? and Time='PM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Secondary Night R25 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "8":
            cur.execute("update R100Shifts set Operator1ID=? where Date=? and Time='AM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Primary Day R100 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "9":
            cur.execute("update R100Shifts set Operator2ID=? where Date=? and Time='AM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Secondary Day R100 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "10":
            cur.execute("update R100Shifts set Operator1ID=? where Date=? and Time='PM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Primary Night R100 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "11":
            cur.execute("update R100Shifts set Operator2ID=? where Date=? and Time='PM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Secondary Night R100 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "12":
            cur.execute("update R114Shifts set Operator1ID=? where Date=? and Time='AM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Primary Day R114 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "13":
            cur.execute("update R114Shifts set Operator2ID=? where Date=? and Time='AM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Secondary Day R114 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "14":
            cur.execute("update R114Shifts set Operator1ID=? where Date=? and Time='PM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Primary Night R114 Operator for date {} to {}".format(getUser(), date, value)))
        if column == "15":
            cur.execute("update R114Shifts set Operator2ID=? where Date=? and Time='PM'", (staffId, dt))
            cur.execute("insert into AuditLogs(LogTime,LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Secondary Night R114 Operator for date {} to {}".format(getUser(), date, value)))
        
        conn.commit()
        conn.close()
    except Error as e:
        print(e)

@eel.expose
def updateComment(date, value):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        dt = datetime.strptime(date, "%d/%m/%Y")
        
        cur = conn.cursor()
        cur.execute("update R25Shifts set Comments=? where date=? and Time='AM'", (value, dt))
        cur.execute("insert into AuditLogs(LogTime, LogMessage) values(?,?)", (datetime.strftime(datetime.utcnow(),"%d/%m/%Y %H:%M:%S"), "{} updated Comments for date {} to '{}'".format(getUser(), date, value)))
        
        conn.commit()
        conn.close()
    except Error as e:
        print(e)
    
@eel.expose
def getMonThuAllocations(dateStart, dateEnd):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        allocations = {}
        
        dateStart = datetime.strptime(dateStart, "%d/%m/%Y")
        dateEnd = datetime.strptime(dateEnd, "%d/%m/%Y")
        cur = conn.cursor()
        cur.execute("select ID, Initials from TVTStaff")
        rows = cur.fetchall()
        for row in rows:
            if row[1] not in allocations:
                allocations[row[1]] = {}
            for building in ["R25", "R100", "R114"]:
                if building not in allocations[row[1]]:
                    allocations[row[1]][building] = {}
                for time in ['AM', 'PM']:
                    if time not in allocations[row[1]][building]:
                        allocations[row[1]][building][time] = 0
                    
        for row in rows:
            for building in ["R25", "R100", "R114"]:
                for time in ['AM', 'PM']:
                    cur.execute("select count(ID) from {}Shifts where Date > ? and Date < ? and cast(strftime('%w',Date) as integer) > 0 and cast(strftime('%w',Date) as integer) < 5 and Date not in (select Date from HolidayDates) and Time=? and (Operator1ID=? or Operator2ID=?)".format(building), (dateStart, dateEnd, time, row[0], row[0]))
                    rows2 = cur.fetchall()
                    for row2 in rows2:
                        allocations[row[1]][building][time] = row2[0]
        conn.close()
        return allocations
    except Error as e:
        print(e)
        
@eel.expose
def getFriAllocations(dateStart, dateEnd):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        allocations = {}
        
        dateStart = datetime.strptime(dateStart, "%d/%m/%Y")
        dateEnd = datetime.strptime(dateEnd, "%d/%m/%Y")
        cur = conn.cursor()
        cur.execute("select ID, Initials from TVTStaff")
        rows = cur.fetchall()
        for row in rows:
            if row[1] not in allocations:
                allocations[row[1]] = {}
            for building in ["R25", "R100", "R114"]:
                if building not in allocations[row[1]]:
                    allocations[row[1]][building] = {}
                for time in ['AM', 'PM']:
                    if time not in allocations[row[1]][building]:
                        allocations[row[1]][building][time] = 0
                    
        for row in rows:
            for building in ["R25", "R100", "R114"]:
                for time in ['AM', 'PM']:
                    cur.execute("select count(ID) from {}Shifts where Date > ? and Date < ? and cast(strftime('%w',Date) as integer) = 5 and Date not in (select Date from HolidayDates) and Time=? and (Operator1ID=? or Operator2ID=?)".format(building), (dateStart, dateEnd, time, row[0], row[0]))
                    rows2 = cur.fetchall()
                    for row2 in rows2:
                        allocations[row[1]][building][time] = row2[0]
        conn.close()
        return allocations
    except Error as e:
        print(e)

@eel.expose
def getWEAllocations(dateStart, dateEnd):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        allocations = {}
        
        dateStart = datetime.strptime(dateStart, "%d/%m/%Y")
        dateEnd = datetime.strptime(dateEnd, "%d/%m/%Y")
        cur = conn.cursor()
        cur.execute("select ID, Initials from TVTStaff")
        rows = cur.fetchall()
        for row in rows:
            if row[1] not in allocations:
                allocations[row[1]] = {}
            for building in ["R25", "R100", "R114"]:
                if building not in allocations[row[1]]:
                    allocations[row[1]][building] = {}
                for time in ['AM', 'PM']:
                    if time not in allocations[row[1]][building]:
                        allocations[row[1]][building][time] = 0
                    
        for row in rows:
            for building in ["R25", "R100", "R114"]:
                for time in ['AM', 'PM']:
                    cur.execute("select count(ID) from {}Shifts where Date > ? and Date < ? and (cast(strftime('%w',Date) as integer) = 0 or cast(strftime('%w',Date) as integer) = 6 or Date in (select Date from HolidayDates)) and Time=? and (Operator1ID=? or Operator2ID=?)".format(building), (dateStart, dateEnd, time, row[0], row[0]))
                    rows2 = cur.fetchall()
                    for row2 in rows2:
                        allocations[row[1]][building][time] = row2[0]
        conn.close()
        return allocations
    except Error as e:
        print(e)

@eel.expose
def getOCAllocations(dateStart, dateEnd):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        allocations = {}
        
        dateStart = datetime.strptime(dateStart, "%d/%m/%Y")
        dateEnd = datetime.strptime(dateEnd, "%d/%m/%Y")
        cur = conn.cursor()
        cur.execute("select ID, Initials from TVTStaff")
        rows = cur.fetchall()
        for row in rows:
            if row[1] not in allocations:
                allocations[row[1]] = {}
                    
        for row in rows:
            cur.execute("select count(ID) from TVTOnCall where Date > ? and Date < ? and (PrimaryID=? or SecondaryID=?)", (dateStart, dateEnd, row[0], row[0]))
            rows2 = cur.fetchall()
            for row2 in rows2:
                allocations[row[1]] = row2[0]
        conn.close()
        return allocations
    except Error as e:
        print(e)

@eel.expose
def getCommitments():
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        commitments = []
        cur = conn.cursor()
        cur.execute("SELECT * from TVTCommitments")
        rows = cur.fetchall()
        for row in rows:
            cur.execute("SELECT Initials from TVTStaff where ID=?", (row[1],))
            initials = cur.fetchone()[0]
            commitments.append((initials, row[2], row[3]))
        conn.close()
        return commitments
    except Error as e:
        print(e)

@eel.expose
def updateCommitment(date, initials, comment):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        cur = conn.cursor()
        cur.execute("select ID from TVTStaff where Initials=?", (initials,))
        staffId = cur.fetchone()[0]
        cur.execute("select ID from TVTCommitments where CommitmentDate=? and TVTStaffID=?", (date, staffId))
        rows = cur.fetchall()
        if comment != "":
            if len(rows) > 0:
                cur.execute("update TVTCommitments set Comment=? where ID=?", (comment, rows[0][0]))
            else:
                cur.execute("insert into TVTCommitments(TVTStaffID, CommitmentDate, Comment) values(?,?,?)", (staffId, date, comment))
        else:
            if len(rows) > 0:
                cur.execute("delete from TVTCommitments where ID=?", (rows[0][0],))
            
        conn.commit()
        conn.close()
    except Error as e:
        print(e)

def getStaffHours():
    allocations = getMonThuAllocations("01/01/2022", "31/12/2022")
    allocations2 = getFriAllocations("01/01/2022", "31/12/2022")
    allocations3 = getWEAllocations("01/01/2022", "31/12/2022")
    hours = {}
    for staff in allocations2:
        hours[staff] = 0
        for building in ["R25", "R100", "R114"]:
            for time in ['AM', 'PM']:
                hours[staff] += int(allocations[staff][building][time])
                hours[staff] += int(allocations2[staff][building][time])
                hours[staff] += int(allocations3[staff][building][time])
    return hours

def getUnavailabilities(date):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        cur.execute("SELECT Initials from TVTStaff where ID in (select TVTStaffID as ID from TVTCommitments where CommitmentDate=?)", (date,))
        rows = cur.fetchall()
        conn.close()
        return rows
    except Error as e:
        print(e)

@eel.expose
def getHolidays():
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        cur = conn.cursor()
        cur.execute("select Date from HolidayDates")
        rows = cur.fetchall()
        holidays = []
        for row in rows:
            date = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")
            holidays.append(int(datetime.timestamp(date)) * 1000)
        conn.close()
        return holidays
    except Error as e:
        print(e)

@eel.expose
def scheduleShifts(R25StartDate, R25Duration, R100StartDate, R100Duration, R114StartDate, R114Duration):
    R25StartDate = datetime.strptime(R25StartDate, "%d/%m/%Y")
    R100StartDate = datetime.strptime(R100StartDate, "%d/%m/%Y")
    R114StartDate = datetime.strptime(R114StartDate, "%d/%m/%Y")
    
    starts = {}
    starts["R25"] = R25StartDate
    starts["R100"] = R100StartDate
    starts["R114"] = R114StartDate
    
    lengths = {}
    lengths["R25"] = int(R25Duration)
    lengths["R100"] = int(R100Duration)
    lengths["R114"] = int(R114Duration)
    
    prev_balances = getStaffHours()
    
    all_buildings = ["R25", "R100", "R114"]
    all_days = pd.date_range(min([starts[b] for b in all_buildings if lengths[b] != 0]), max([starts[b] + timedelta(days=lengths[b] - 1) for b in all_buildings]), freq="d")
    all_shifts = ["AM", "PM"]
    
    starts['On Call'] = min(all_days)
    lengths['On Call'] = len(all_days)
    
    unavailabilities = {}
    for p in prev_balances:
        for d in all_days:
            for s in all_shifts:
               unavailabilities[(p,d,s)] = False
    
    for dt in all_days:
        date = datetime.strftime(dt, "%d/%m/%Y")
        unavailable = getUnavailabilities(date)
        for staff in unavailable:
            unavailabilities[(staff[0], dt, "AM")] = True
            unavailabilities[(staff[0], dt, "PM")] = True
    
    for dt in all_days:
        date = datetime.strftime(dt, "%d/%m/%Y")
        for staff in ["EJ", "RS", "EG", "CJW", "JP"]:
            unavailabilities[(staff, dt, "PM")] = True
    
    for dt in all_days:
        date = datetime.strftime(dt, "%d/%m/%Y")
        unavailabilities[("CJW", dt, "AM")] = True
    
    for dt in all_days:
        date = datetime.strftime(dt, "%Y-%m-%d %H:%M:%S")
        shiftRow = shiftData.loc[shiftData.Date == date]
        for building in all_buildings:
            for time in ["Day", "Night"]:
                shift_name1 = "{} {} Primary".format(building, time)
                shift_name2 = "{} {} Secondary".format(building, time)
                initials = shiftRow[shift_name1].values[0]
                initials2 = shiftRow[shift_name2].values[0]
                if initials != "" and initials != None:
                    if time == "Day":
                        unavailabilities[(initials, dt, "AM")] = True
                    if time == "Night":
                        unavailabilities[(initials, dt, "PM")] = True
                if initials2 != "" and initials2 != None:
                    if time == "Day":
                        unavailabilities[(initials2, dt, "AM")] = True
                    if time == "Night":
                        unavailabilities[(initials2, dt, "PM")] = True
    
    shifts = scheduler.run(prev_balances, unavailabilities, all_buildings, [p for p in prev_balances], all_days, all_shifts, starts, lengths)
    
    return shifts
    

@eel.expose
def getMonthlyShifts():
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        name = getUser()
        
        allocations = [0 for month in range(12)]
        
        cur = conn.cursor()
        cur.execute("select ID, Initials from TVTStaff where LongName=?", (name,))
        rows = cur.fetchall()
        year = datetime.utcnow().year
        for month in range(1,13):
            for row in rows:
                for building in ["R25", "R100", "R114"]:
                    lastDay = calendar.monthrange(year, month)[1]
                    dateStart = datetime.strptime("01/{:02d}/{} 00:00:00".format(month, year), "%d/%m/%Y %H:%M:%S")
                    dateEnd = datetime.strptime("{}/{:02d}/{} 23:59:59".format(lastDay, month, year), "%d/%m/%Y %H:%M:%S")
                    cur.execute("select count(ID) from {}Shifts where Date >= ? and Date <= ? and (Operator1ID=? or Operator2ID=?)".format(building), (dateStart, dateEnd, row[0], row[0]))
                    rows2 = cur.fetchall()
                    for row2 in rows2:
                        allocations[month-1] += row2[0]
        conn.close()
        return allocations
    except Error as e:
        print(e)
    
@eel.expose
def getTotalShifts():
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        name = getUser()
        
        allocations = 0
        
        cur = conn.cursor()
        cur.execute("select ID, Initials from TVTStaff where LongName=?", (name,))
        rows = cur.fetchall()
        year = datetime.utcnow().year
        for row in rows:
            for building in ["R25", "R100", "R114"]:
                dateStart = datetime.strptime("01/01/{} 00:00:00".format(year), "%d/%m/%Y %H:%M:%S")
                dateEnd = datetime.strptime("01/12/{} 23:59:59".format(year), "%d/%m/%Y %H:%M:%S")
                cur.execute("select count(ID) from {}Shifts where Date >= ? and Date <= ? and (Operator1ID=? or Operator2ID=?)".format(building), (dateStart, dateEnd, row[0], row[0]))
                rows2 = cur.fetchall()
                for row2 in rows2:
                    allocations += row2[0]
        conn.close()
        
        return allocations
    except Error as e:
        print(e)

@eel.expose
def getContactDetails():
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
        cur = conn.cursor()
        cur.execute("select * from ContactInfo")
        rows = cur.fetchall()
        conn.close()
        return rows
    except Error as e:
        print(e)

print("TVT Shift Planner Console")
print("-------------------------")
eel.start('index.html', mode="chrome-app", cmdline_args=['--start-fullscreen', '--browser-startup-dialog'])