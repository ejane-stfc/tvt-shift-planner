# -*- coding: utf-8 -*-
"""
Created on Mon Feb 14 12:31:50 2022

@author: ymk91659
"""

from ortools.sat.python import cp_model
from datetime import datetime, timedelta
import pandas as pd


# print("Press CTRL+C to exit at any time")
# print("""
# |---------------------------------------------------------------------------------------|
# |                                   Constraints                                         |
# |---------------------------------------------------------------------------------------|
# | * Each staff member should have equal shifts if possible                              |
# | * Each staff member should not be on nights for more than 5 consecutive shifts        |
# | * Optimise for 3-4 nights per night shift block                                       |
# | * Each staff member should have a 2 days off after a night shift                      |
# | * Cannot be on day and night shift on the same day                                    |
# | * Enforces unavailabilities (set to all Graduates + Sean unavailable for nights)      |
# | * Takes previous shift balances into account                                          |
# | * Maximum 5 shifts per 7 day period                                                   |
# |---------------------------------------------------------------------------------------|
# """)

def negated_bounded_span(works, start, length, b, p, n_days, all_days, time):
    sequence = []
    # Left border (start of works, or works[start - 1])
    if start > all_days[0]:
        sequence.append(works[(b, p, start - timedelta(days=1), time)])
    for i in range(length):
        sequence.append(works[(b, p,start + timedelta(days=i), time)].Not())
    # Right border (end of works or works[start + length])
    if start + timedelta(days=length) < all_days[-1]:
        sequence.append(works[(b, p, start + timedelta(days=length), time)])
    return sequence

def run(prev_balances, unavailabilities, all_buildings, all_staff, all_days, all_shifts, starts, lengths):
    model = cp_model.CpModel()
    
    n_days = len(all_days)
    
    costs_vars = []
    costs = []
    
    shiftSums = {}
    for p in all_staff:
        shiftSums[p] = model.NewIntVar(0, 366, "{} Shifts".format(p))
    
    maxShifts = model.NewIntVar(0, 10000, "maxShifts")
    minShifts = model.NewIntVar(0, 10000, "minShifts")
    
    #Initialise the shifts
    shifts = {}
    for b in all_buildings:
        for p in all_staff:
            for d in all_days:
                for s in all_shifts:
                   shifts[(b,p,d,s)] = model.NewBoolVar("shift with id " + str(b) + " " + str(p) + " " + str(d) + " " + str(s))
    for p in all_staff:
        for d in all_days:
            shifts[("On Call", p,d,"24h")] = model.NewBoolVar("shift with id On Call " + str(p) + " " + str(d) + " 24h")
    
    
    #Each shift can only be worked by one person
    for b in all_buildings:
        for d in all_days:
            for s in all_shifts:
                if d >= starts[b] and d < starts[b] + timedelta(days=lengths[b]):
                    model.Add(sum(shifts[(b,p,d,s)] for p in all_staff) == 1)
                else:
                    model.Add(sum(shifts[(b,p,d,s)] for p in all_staff) == 0)

    #Only one person on call
    for d in all_days:
        if d >= starts["On Call"] and d < starts["On Call"] + timedelta(days=lengths["On Call"]):
            model.Add(sum(shifts[("On Call",p,d,"24h")] for p in all_staff) == 1)
        else:
            model.Add(sum(shifts[("On Call",p,d,"24h")] for p in all_staff) == 0)
    
    #enforce unavailabilities
    for p in all_staff:
        for d in all_days:
            for s in all_shifts:
                if unavailabilities[(p,d,s)] == True:
                    for b in all_buildings:
                        model.Add(shifts[(b,p,d,s)] == 0)
    for p in all_staff:
        for d in all_days:
            if unavailabilities[(p,d,"AM")] == True or unavailabilities[(p,d,"PM")] == True:
                model.Add(shifts[("On Call",p,d,"24h")] == 0)
    for p in ["EJ", "EG", "RS", "CJW", "JP", "KG", "SS", "CS", "MR"]:
        for d in all_days:
            model.Add(shifts[("On Call",p,d,"24h")] == 0)
    
    #first rest day
    for p in all_staff:
        for d in all_days[1:]:
            for b in all_buildings:
                for b2 in all_buildings:
                    model.Add(shifts[(b,p,d-timedelta(days=1),"PM")] != shifts[(b2,p,d,"AM")]).OnlyEnforceIf(shifts[(b,p,d-timedelta(days=1),"PM")])
    
    #second rest day
    for p in all_staff:
        for d in all_days[2:]:
            for b in all_buildings:
                for b2 in all_buildings:
                    model.Add(shifts[(b,p,d-timedelta(days=2),"PM")] != shifts[(b2,p,d,"AM")]).OnlyEnforceIf(shifts[(b,p,d-timedelta(days=2),"PM")])
    
    #max 5 shifts per 7 day period
    for p in all_staff:
        for d in range(7,len(all_days)):
            model.Add(sum(shifts[(b,p,day,s)] for day in all_days[d-7:d] for s in all_shifts for b in all_buildings) <= 5)
    
    # only one shift per day
    for p in all_staff:
        for d in all_days:
            model.Add(sum(shifts[(b,p,d,s)] for s in all_shifts for b in all_buildings) + shifts[("On Call", p, d, "24h")] <= 1)
    
    #hard night shift maximum
    for p in all_staff:
        for b in all_buildings:
            for start in range(n_days - 5):
                model.AddBoolOr([shifts[(b,p,all_days[i],"PM")].Not() for i in range(start, start + 6)])

    #hard on call maximum
    for p in all_staff:
            for start in range(n_days - 1):
                model.AddBoolOr([shifts[("On Call",p,all_days[i],"24h")].Not() for i in range(start, start + 2)])
    
    #hard night shift minimum
    for p in all_staff:
        for b in all_buildings:
            for length in range(1, 2):
                for start in range(n_days - length + 1):
                    model.AddBoolOr(negated_bounded_span(shifts, all_days[start], length, b, p, n_days, all_days, "PM"))
    
    #night shift soft minimum
    for p in all_staff:
        for b in all_buildings:
            for length in range(2, 4):
                for start in range(n_days - length + 1):
                    span = negated_bounded_span(shifts, all_days[start], length, b, p, n_days, all_days, "PM")
                    name = 'under_span(start=%i, length=%i, time=%s)' % (start, length, 'AM')
                    lit = model.NewBoolVar(name)
                    span.append(lit)
                    model.AddBoolOr(span)
                    costs_vars.append(lit)
                    costs.append(2 * (3 - length))
    
    #night shift soft maximum
    for p in all_staff:
        for b in all_buildings:
            for length in range(5, 6):
                for start in range(n_days - length + 1):
                    span = negated_bounded_span(shifts, all_days[start], length, b, p, n_days, all_days, "PM")
                    name = 'over_span(start=%i, length=%i, time=%s)' % (start, length, 'AM')
                    lit = model.NewBoolVar(name)
                    span.append(lit)
                    model.AddBoolOr(span)
                    costs_vars.append(lit)
                    costs.append(0.1*(length - 4))
                    
    #day shift soft maximum
    for p in all_staff:
        for b in all_buildings:
            for length in range(2, 6):
                for start in range(n_days - length + 1):
                    span = negated_bounded_span(shifts, all_days[start], length, b, p, n_days, all_days, "AM")
                    name = 'over_span(start=%i, length=%i, time=%s)' % (start, length, 'AM')
                    lit = model.NewBoolVar(name)
                    span.append(lit)
                    model.AddBoolOr(span)
                    costs_vars.append(lit)
                    costs.append(0.1*(length - 1))
    
    # assign allocations
    for p in all_staff:
        model.Add(shiftSums[p] == sum(shifts[(b,p,d,"AM")] for d in all_days for b in all_buildings) + sum(shifts[(b,p,d,"PM")] for d in all_days for b in all_buildings) + prev_balances[p])
    
    model.AddMaxEquality(maxShifts, shiftSums.values())
    model.AddMinEquality(minShifts, shiftSums.values())
    
    #minimise difference in shifts and penalties for night shift limits
    model.Minimize((maxShifts-minShifts)+sum(costs_vars[i] * costs[i] for i in range(len(costs_vars))))
    
    # solve the model
    solver = cp_model.CpSolver()
    solver.parameters.linearization_level = 0
    solver.parameters.max_time_in_seconds = 30.0
    status = solver.Solve(model)
    if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
        final_shifts = {}
        for p in all_staff:
            final_shifts[p] = prev_balances[p]
        
        scheduledShifts = {"On Call": {}, "R25": {}, "R100": {}, "R114": {}}
        
        for d in all_days:
            found = False
            for p in all_staff:
                if solver.Value(shifts[("On Call",p,d,"24h")]) == 1:
                    found = True
                    scheduledShifts["On Call"][datetime.strftime(d, "%d/%m/%Y")] = p
            if found == False:
                scheduledShifts["On Call"][datetime.strftime(d, "%d/%m/%Y")] = ""
        for b in all_buildings:
            for d in all_days:
                scheduledShifts[b][datetime.strftime(d, "%d/%m/%Y")] = {}
                for s in all_shifts:
                    found = False
                    for p in all_staff:
                        if solver.Value(shifts[(b,p,d,s)]) == 1:
                            found = True
                            scheduledShifts[b][datetime.strftime(d, "%d/%m/%Y")][s] = p
                            final_shifts[p] += 1
                    if found == False:
                        scheduledShifts[b][datetime.strftime(d, "%d/%m/%Y")][s] = ""

        # print("On Call".center(43," "))
        # print(u"\u2014" * 43)
        # print("|" + "Day".center(20, " ") + "|" + "24h".center(20, " "), end="|\n")
        # print(u"\u2014" * 43)
        # for d in all_days:
        #     print("|" + str(d.strftime("%d/%m/%Y")).center(20," "), end="")
        #     print("|", end="")
        #     found = False
        #     for p in all_staff:
        #         if solver.Value(shifts[("On Call",p,d,"24h")]) == 1:
        #             found = True
        #             print(p.center(20, " "), end="|")
        #     if found == False:
        #         print("None".center(20, " "), end="|")
        #     print()
        # print(u"\u2014"*43)
        # print()
        # for b in all_buildings:
        #     print(b.center(64," "))
        #     print(u"\u2014" * 64)
        #     print("|" + "Day".center(20, " ") + "|" + "Day Shift".center(20, " ") + "|" + "Night Shift".center(20, " "), end="|\n")
        #     print(u"\u2014" * 64)
        #     for d in all_days:
        #         print("|" + str(d.strftime("%d/%m/%Y")).center(20," "), end="")
        #         print("|", end="")
        #         for s in all_shifts:
        #             found = False
        #             for p in all_staff:
        #                 if solver.Value(shifts[(b,p,d,s)]) == 1:
        #                     found = True
        #                     print(p.center(20, " "), end="|")
        #                     final_shifts[p] += 1
        #             if found == False:
        #                 print("None".center(20, " "), end="|")
        #         print()
        #     print(u"\u2014"*64)
        #     print()
        # print("Final Shift Balances:")
        # print(final_shifts)
        
        return scheduledShifts
    else:
        print("No Solution Found in the Time Limit")
        return []

# if __name__ == "__main__":
#     all_buildings = ["R25", "R100", "R114"]
#     all_staff = ["Ed", "Jago", "Dan", "Sean", "Jack", "Rob", "Simon", "Kevin", "Emre", "Liam", "Max", "Conor", "Chris"]
#     all_shifts = ["day", "night"]
#     while True:
#         print()
#         starts = {}
#         lengths = {}
#         for b in all_buildings:
#             starts[b] = datetime.strptime(input("Enter the start day of {} shifts (dd/mm/yyyy): ".format(b)), "%d/%m/%Y")
#             lengths[b] = int(input("Enter the length of {} shifts: ".format(b)))
#         print()
        
#         starts["On Call"] = min(starts.values())
#         lengths["On Call"] = (max([starts[b] + timedelta(days=lengths[b]) for b in all_buildings]) - starts["On Call"]).days
        
#         all_days = pd.date_range(min([starts[b] for b in all_buildings]), max([starts[b] + timedelta(days=lengths[b] - 1) for b in all_buildings]), freq="d")
        
#         prev_balances = {}
#         for p in all_staff:
#             prev_balances[p] = 0
        
#         unavailabilities = {}
#         for p in all_staff:
#             for d in all_days:
#                 for s in all_shifts:
#                    unavailabilities[(p,d,s)] = False
        
#         for d in all_days:
#             unavailabilities[("Sean", d, "night")] = True
#             unavailabilities[("Chris", d, "night")] = True
#             unavailabilities[("Ed", d, "night")] = True
#             unavailabilities[("Jack", d, "night")] = True
#             unavailabilities[("Rob", d, "night")] = True
#             unavailabilities[("Emre", d, "night")] = True
        
#         for d in all_days:
#             unavailabilities[("Chris", d, "night")] = True
#             unavailabilities[("Ed", d, "night")] = True
#             unavailabilities[("Jack", d, "night")] = True
#             unavailabilities[("Rob", d, "night")] = True
#             unavailabilities[("Emre", d, "night")] = True
#             unavailabilities[("Kevin", d, "night")] = True
#             unavailabilities[("Max", d, "night")] = True
#             unavailabilities[("Conor", d, "night")] = True
        
#         run(prev_balances, unavailabilities, all_buildings, all_staff, all_shifts, starts, lengths)