let chartOptions = {
	'monthlyShifts': {
		series: [{
			data: [10,11,1,15,12,5,9,4,0,8,14,1]
		}],
		chart: {
			type: 'bar',
			height: 350
		},
		plotOptions: {
			bar: {
				borderRadius: 10,
				dataLabels: {
					position: 'top',
				},
			}
		},
		dataLabels: {
			enabled: true,
			formatter: function (val) {
				return val + "%";
			},
			offsetY: -20,
			style: {
				fontSize: '12px',
				colors: ["#304758"]
			}
		},
		xaxis: {
			categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			position: 'top',
			axisBorder: {
				show: false
			},
			axisTicks: {
				show: false
			},
			crosshairs: {
				fill: {
					type: 'gradient',
					gradient: {
						colorFrom: '#D8E3F0',
						colorTo: '#BED1E6',
						stops: [0, 100],
						opacityFrom: 0.4,
						opacityTo: 0.5,
					}
				}
			},
			tooltip: {
				enabled: true,
			}
		},
		yaxis: {
			axisBorder: {
				show: false
			},
			axisTicks: {
				show: false,
			},
			labels: {
				show: false,
				formatter: function (val) {
					return val + "%";
				}
			}
		},
	}
};

const renderedCharts = [];
function updateCharts() {
	if (typeof ApexCharts !== 'function') {
			return;
	}

	const chartElements = document.querySelectorAll('.chart');

	chartElements.forEach((chartElement) => {
		if (renderedCharts.includes(chartElement)) {
			return;
		}

		const type = chartElement.getAttribute('data-type');
		const variant = chartElement.getAttribute('data-variant');

		if (typeof chartOptions[type] === 'object') {
			let options = demoOptions[type];

			if (typeof options['__variants'] === 'object' && typeof options['__variants'][variant] === 'object') {
				options = { ...options, ...options['__variants'][variant] };
			}

			const chart = new ApexCharts(chartElement, options);
			chart.render();
			renderedCharts.push(chartElement);
		}
	});
}

updateCharts();
