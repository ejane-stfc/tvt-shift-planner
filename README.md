# README #

This repository holds the code for the TVT Shift Planner

### What is this repository for? ###

This repository is for keeping track of updates and development to the TVT Shift Planner. All development changes should be made to the 'develop' branch before being merged in a release to the 'main' branch.

### Who do I talk to? ###

This repository is maintained by Edward Jane.